 import { useEffect, useState, useContext } from 'react';
import OrderCard from '../components/OrderCard';
import UserContext from '../UserContext';

export default function Orders() {
  const [orders, setOrders] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    if (user) {
      fetch(`https://capstone-3-bautista-backend.onrender.com/orders/cart/${user.id}`)
        .then((res) => res.json())
        .then((data) => {
          console.log(data); // Add this line to log the value of data
          const orderCards = data.map((order) => (
            <OrderCard orderProp={order} key={order._id} />
          ));
          setOrders(orderCards);
        })
        .catch((error) => console.error(error));
    }
  }, [orders]);

  return (
    <>
      <h1>Orders</h1>
      {orders}
    </>
  );
}
