import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Button, Form } from 'react-bootstrap';

function Logout() {
  const { setUser } = useContext(UserContext);
  const [feedbackSent, setFeedbackSent] = useState(false);
  const [feedbackMessage, setFeedbackMessage] = useState('');

  const handleLogout = () => {
    localStorage.removeItem('user');
    setUser({ id: null, name: null });
    window.location.href = '/';
  };

  const sendFeedback = async () => {
    // Send the feedback message using a server-side technology such as Node.js or PHP
    const response = await fetch('/send-feedback', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ email: 'ne.bautista@gmail.com', message: feedbackMessage })
    });

    if (response.ok) {
      setFeedbackSent(true);
    }
  };

  const handleFeedbackChange = (event) => {
    setFeedbackMessage(event.target.value);
  };

  return (
    <div className="text-center">
      <div className="mt-5 text-center">
        <h2>Thank you for using our platform!</h2>
        <p>We hope you found it useful and that it met your needs. We appreciate your business and look forward to serving you again in the future. Please don't hesitate to reach out if you have any questions or concerns. Have a great day!</p>
        {!feedbackSent && (
          <Form>
            <Form.Group controlId="formFeedback">
              <Form.Label>Send a Feedback</Form.Label>
              <Form.Control as="textarea" rows={3} value={feedbackMessage} onChange={handleFeedbackChange} style={{ width: '50vw' }} className="mx-auto" />
            </Form.Group>
            <Button variant="primary" className="mt-2" onClick={sendFeedback}>
              Submit Feedback
            </Button>
          </Form>
        )}
        {feedbackSent && <p>Thank you for your feedback!</p>}
      </div>
      <Link to="/logout" onClick={handleLogout} style={{ textDecoration: 'none' }} >
        <Button variant="danger" className="mt-5 mx-auto">Confirm Logout</Button>
      </Link>
    </div>
  );
}

export default Logout;
