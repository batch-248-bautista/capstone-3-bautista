//import useState and useEffect from react
/*import { useState, useEffect } from 'react';*/
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//define state hooks for all input fields

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	//add an "isActive" state for conditional rendering of the submit button
	//state to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(mobileNo);
	console.log(password1);
	console.log(password2);

	//Function to simulate user registration

	function registerUser(e){

		//prevents page redirection via form submission
	e.preventDefault();

	//create an object with the user registration data
	const userData = {
		firstName,
		lastName,
		email,
		mobileNo,
		password: password1
	};

	//make an HTTP POST request to your backend API
	fetch('https://capstone-3-bautista-backend.onrender.com/users/register', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(userData)
	})
	.then(response => response.json())
	.then(data => {
		//clear input fields
		setFirstName("");
		setLastName("");
		setEmail("");
		setMobileNo("");
		setPassword1("");
		setPassword2("");

		//show a success message
	    Swal.fire({
	        title: 'Success',
	        text: 'Thank you for signing up! Please log in using the email you have registered.',
	        icon: 'success'
	    });

	    // redirect to products page
    	navigate('/login');

	})
	.catch(error => {
		console.error('Error:', error);
		Swal.fire({
		title: 'Error',
		text: 'An error occurred. Please try again later.',
		icon: 'error'
		});
	});

	}
	//Define a useEffect for validating user input

	useEffect(()=>{

		//validation to enable the submit button
		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo.length === 11 && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[firstName, lastName, email, mobileNo, password1, password2])


	return (

		(user.id !== null) ?
		<Navigate to="/"/>
		:

	/*
		Bind the user input states into their corresponding input fields via "onChange" event handler and set the value of the form input fields via 2-way binding
	*/

	<>
		<h1>Register</h1>

		{/*
			- The onChange event is triggered whenever the value of the input field changes. When this happens, the arrow function is called with the event object as its parameter
			- The arrow function then extracts the current value of the input field using e.target.value and passes it to the setFirstName function.
		*/}

		<Form onSubmit={(e)=>registerUser(e)}>
		  <Form.Group className="mb-3" controlId="firstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control
	        	type="text"
	        	placeholder="Enter First Name"
	        	value = {firstName}
	        	onChange = {e=>setFirstName(e.target.value)}
	        	required/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control
	        	type="text"
	        	placeholder="Enter Last Name"
	        	value = {lastName}
	        	onChange = {e=>setLastName(e.target.value)}
	        	required/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email Address</Form.Label>
	        <Form.Control
	        	type="email"
	        	placeholder="Enter Email"
	        	value = {email}
	        	onChange = {e=>setEmail(e.target.value)}
	        	required/>
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="mobileNo">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control
	        	type="text"
	        	placeholder="Enter Mobile Number"
	        	value = {mobileNo}
	        	onChange = {e=>setMobileNo(e.target.value)}
	        	required/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control
	        	type="password"
	        	placeholder="Password"
	        	value = {password1}
	        	onChange = {e=>setPassword1(e.target.value)}
	        	required/>
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control
	        	type="password"
	        	placeholder="Verify Password"
	        	value = {password2}
	        	onChange = {e=>setPassword2(e.target.value)}
	        	required/>
	      </Form.Group>

	      {isActive ?

	      	<Button variant="primary" type="submit" id="submitBtn">
	       		Submit
	      	</Button>

	      	:

	      	<Button variant="primary" type="submit" id="submitBtn" disabled>
	        	Submit
	      	</Button>

	      }

	    </Form>
	 </>
	)
}
