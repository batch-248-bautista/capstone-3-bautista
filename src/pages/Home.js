import Banner from '../components/Banner';

const data = {
	title: "Green Leaf Shop by NB!",
	content: "We offer a wide variety of fresh and delicious green leafy vegetables, dressing for salads, and other fruits for salads. Make your salads more exciting and nutritious!",
	destination: "/products",
	label: "Shop now!"
}


export default function Home(){

	return (
		<>	
			<Banner data={data}/>
			
    		{/*<Highlights/>*/}
    		{/*<CourseCard/>*/}
		</>
	)
}

