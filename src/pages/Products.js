import {useEffect, useState, useContext} from 'react';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';


export default function Products(){

  const [products, setProducts] = useState([]);

  const {user} = useContext(UserContext);

  useEffect(()=>{

    fetch(`https://capstone-3-bautista-backend.onrender.com/products/active`)
    .then(res =>res.json())
    .then(data =>{
      console.log(data);

      const productArr = (data.map((product)=>{


        return(

          <ProductCard productProp={product} key={product._id}/>
        )
      }))
      setProducts(productArr)
    })
  },[products])



  return(
    <>
      <h1>Products</h1>
      
      {products}
    </>

  )


}
