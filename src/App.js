import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';

import { BrowserRouter as Router } from 'react-router-dom';

import {Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Dash from './components/Dash';
import AddProduct from './components/AddProduct';
import EditProduct from './components/EditProduct';
import Users from './components/Users';
import Orders from './components/Orders';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Cart from './pages/Cart';
import OrderView from './components/OrderView';
/*import CancelOrder from './components/CancelOrder';*/
/*import Contacts from './components/Contacts';*/

import './App.css';

//import UserProvider from userCOntext
import {UserProvider} from './UserContext';


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin:null
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{
    fetch(`https://capstone-3-bautista-backend.onrender.com/users/details`,{
      headers:{
        Authorization:`Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data => {
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser ({
          id : data._id,
          isAdmin : data.isAdmin
        })
      }
      else{
        setUser ({
          id : null,
          isAdmin : null
        })
      }
    })
  },[])

  return (
    <UserProvider value={{ user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            <Route path="/" element={<Home/>} />
            {/*<Route path="/courses" element={<Courses/>} />
            <Route path="/courses/:courseId" element={<CourseView/>}/>*/}
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/register" element={<Register/>} />
            {/*<Route path="*" element={<NotFound/>} />*/}
            <Route path="/admin" element={<Dash/>}/>
            <Route path="/addProduct" element={<AddProduct/>} />
            <Route path="/editProduct/:productId" element={<EditProduct/>} />
            <Route path="/users" element={<Users/>} />
            <Route path="/orders" element={<Orders/>} />
            <Route path="/products" element={<Products/>} />
            <Route path="/products/:productId" element={<ProductView/>} />
            <Route path="/cart" element={<Cart/>} />
            <Route path="/orders/:orderId" element={<OrderView/>} />
            
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
