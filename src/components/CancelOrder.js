import { useState } from "react";
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
function OrderView({ orderId }) {
  const [deleteError, setDeleteError] = useState(null);

  const handleDelete = async () => {
    try {
      const response = await fetch(`https://capstone-3-bautista-backend.onrender.com/orders/cancelorder/${orderId}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')} `,
        },
      });

      if (response.ok) {
        // Order deleted successfully, navigate to some other page
      } else {
        // Something went wrong, handle the error
        setDeleteError("Failed to delete order.");
      }
    } catch (error) {
      // Something went really wrong, handle the error
      setDeleteError(error.message);
    }
  };

  return (
    <div>
      <Link to="/cart" onClick={handleDelete} style={{ textDecoration: 'none' }} >
        <Button variant="danger" className="mt-5 mx-auto">Confirm to cancel</Button>
      </Link>
    </div>
  );
}

export default OrderView;
