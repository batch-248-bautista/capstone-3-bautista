import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Orders(){

  // to validate the user role.
  const {user} = useContext(UserContext);

  //Create allOrders State to contain the orders from the database.
  const [allOrders, setAllOrders] = useState([]);

  //"fetchData()" wherein we can invoke if there is a certain change with the orders.
  const fetchData = () =>{
    // Get all orders in the database
    fetch(`https://capstone-3-bautista-backend.onrender.com/orders/`,{
			headers:{

				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setAllOrders(data.map(order => {
        return(
          <tr key={order._id}>
            <td>{order._id}</td>
            <td>{order.userId}</td>
            <td>{order.totalAmount}</td>
            <td>{order.purchasedOn}</td>
            <td>
              <ul>
                {order.products.map(product => {
                  return (
                    <li key={product._id}>
                      <h6>{product.name}</h6>
                      <p>Product ID: {product.productId}</p>
                      <p>Quantity: {product.quantity}</p>
                      <p>Price: {product.subtotal}</p>
                    </li>
                  )
                })}
              </ul>
            </td>
            <td>{order.isPaid ? "Paid" : "Pending"}</td>
            
            <td>
              {
                // We use conditional rendering to set which button should be visible based on the order status (paid/pending)
                (order.isPaid)
                ? 
                  // A button to change the order status to "Pending"
                  <Button variant="danger" size="sm" onClick ={() => pending(order._id, order.userId)}>Set as Pending</Button>
                :
                  <>
                    {/* A button to change the order status to "Paid"*/}
                    <Button variant="success" size="sm" onClick ={() => paid(order._id, order.userId)}>Set as Paid</Button>
                    
                  </>
              }
            </td>
          </tr>
        )
      }))

    })
  }

	//payment status "pending"
	const pending = (orderId, userId) =>{
		console.log(orderId);
		console.log(userId);

		fetch(`https://capstone-3-bautista-backend.onrender.com/orders/paymentstatuspending/${orderId}`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isPaid: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${userId} Payment Status : PENDING`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//payment status "paid"
	const paid = (orderId, userId) =>{
		console.log(orderId);
		console.log(userId);

		fetch(`https://capstone-3-bautista-backend.onrender.com/orders/paymentstatuspaid/${orderId}`,{
			method: "PATCH",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isPaid: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${userId} Payment Status : PAID.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	// To fetch all courses in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all courses.
		fetchData();
	})
	//***[] dependencies are optional

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<h2> Orders </h2>
				{/*A button to add a new course*/}
				<Button as={Link} to="/admin" variant="primary" size="md" className="mx-2">Go Back</Button>
				{/*<Button variant="success" size="lg" className="mx-2" disabled>Show Enrollments</Button>*/}
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>ID</th>
		         <th>User ID</th>
		         <th>Total Amount</th>
		         <th>Purchased Date</th>
		         <th>Product Orders</th>
		         <th>Payment Status</th>
		         <th>Actions</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}
