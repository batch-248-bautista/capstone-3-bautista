import { useState } from 'react';
import { Form, InputGroup, Button } from 'react-bootstrap';

export default function ProductSearch({ onSearch }) {
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = () => {
    onSearch(searchTerm);
  };

  const handleInputChange = (event) => {
    setSearchTerm(event.target.value);
  };

  return (
    <Form>
      <InputGroup className="mb-3">
        <Form.Control
          type="text"
          placeholder="Search products"
          value={searchTerm}
          onChange={handleInputChange}
        />
        <Button variant="outline-secondary" onClick={handleSearch}>
          <i className="bi bi-search"></i>
        </Button>
      </InputGroup>
    </Form>
  );
}
