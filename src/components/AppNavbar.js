import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import logo from '../images/logo.png';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { FaShoppingCart } from 'react-icons/fa';

function AppNavbar() {
  const { user, unsetUser } = useContext(UserContext);

  const handleLogout = () => {
    localStorage.removeItem("user");
    unsetUser();
  };

  const loggedInMenu = (
    <NavDropdown title={`Welcome!`} id="basic-nav-dropdown">
      {/*<NavDropdown.Item as={Link} to="/profile">Profile</NavDropdown.Item>*/}
      {/*<NavDropdown.Item as={Link} to="/orders">Orders</NavDropdown.Item>*/}
      <NavDropdown.Item as={Link} to="/logout" onClick={handleLogout}>Logout</NavDropdown.Item>
    </NavDropdown>
  );

  const loggedOutMenu = (
    <NavDropdown title="Sign in / Register" id="basic-nav-dropdown">
      <NavDropdown.Item as={Link} to="/login">Login</NavDropdown.Item>
      <NavDropdown.Item as={Link} to="/register">Register</NavDropdown.Item>
    </NavDropdown>
  );

  const isRegularUser = user && user.Role !== 'admin';
  const cartIcon = isRegularUser ? (
    <Nav.Link as={Link} to="/cart">
      <FaShoppingCart className="me-1" />
      Cart
    </Nav.Link>
  ) : null;

  return (
    <Navbar expand="md" sticky-top>
      <Container>
        <Navbar.Brand href="#home" className="d-flex align-items-center">
          <img
            alt=""
            src={logo}
            width="50"
            height="50"
            className="d-inline-block align-top me-2"
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse className="justify-content-end collapse">
          <Nav className="justify-content-end">
            <Nav.Link as={Link} to="/">Home</Nav.Link>
            <Nav.Link as={Link} to="/products">Products</Nav.Link>
            {/*<Nav.Link as={Link} to="/contacts">Contact</Nav.Link>*/}
            
            {user && user.id !== null ? (user.isAdmin ? null : (
              <>
                {cartIcon}
                {loggedInMenu}
              </>
            )) : loggedOutMenu}
            {user && user.isAdmin && loggedInMenu}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
export default AppNavbar;
