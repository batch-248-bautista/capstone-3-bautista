import { useContext, useState, useEffect } from "react";
import {Table, Button} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Users(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	//Create allProducts State to contain the products from the database.
	const [allUsers, setAllUsers] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the product.
	const fetchData = () =>{
		// Get all users in the database
		fetch(`https://capstone-3-bautista-backend.onrender.com/users/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.password}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "Admin" : "Regular"}</td>
						
						
						<td>
							{
								// We use conditional rendering to set which button should be visible based on the product status (active/inactive)
								(user.isAdmin)
								?	
								 	// A button to change the product status to "Inactive"
									<Button variant="danger" size="sm" onClick ={() => removeAsAdmin(user._id, user.firstName)}>Remove as Admin</Button>
								:
									<>
										{/* A button to change the product status to "Active"*/}
										<Button variant="success" size="sm" onClick ={() => setAsAdmin(user._id, user.firstName)}>Set as Admin</Button>
										
									</>
							}
						</td>
					</tr>
				)
			}))

		})
	}

	//remove the user as an admin
	const removeAsAdmin = (userId, firstName) =>{
		console.log(userId);
		console.log(firstName);

		fetch(`https://capstone-3-bautista-backend.onrender.com/users/removeadmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${firstName} removed as an Admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the course active
	const setAsAdmin = (userId, firstName) =>{
		console.log(userId);
		console.log(firstName);

		fetch(`https://capstone-3-bautista-backend.onrender.com/users/setadmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${firstName} is now an Admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	// To fetch all courses in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all courses.
		fetchData();
	})
	//***[] dependencies are optional

	return(
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center">
				<h1>Admin Dashboard</h1>
				<h2> Users </h2>
				{/*A button to add a new course*/}
				<Button as={Link} to="/admin" variant="primary" size="md" className="mx-2">Go Back</Button>
				{/*<Button variant="success" size="lg" className="mx-2" disabled>Show Enrollments</Button>*/}
			</div>
			<Table striped bordered hover>
		     <thead>
		       <tr>
		         <th>ID</th>
		         <th>First Name</th>
		         <th>Last Name</th>
		         <th>Email</th>
		         <th>Password</th>
		         <th>Mobile No.</th>
		         <th>Role</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
		     </tbody>
		   </Table>
		</>
		:
		<Navigate to="/products" />
	)
}
