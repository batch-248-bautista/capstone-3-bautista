import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { productId } = useParams();
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [isActive, setIsActive] = useState(false); // added state for product active status
  const [userId, setUserId] = useState(null);
  const [orders, setOrders] = useState([]);

  const order = (productId, quantity) => {
    fetch(`https://capstone-3-bautista-backend.onrender.com/orders/addtocart`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')} `
      },
      body: JSON.stringify({
        productId: productId,
        quantity: quantity,
        userId: userId
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data)
        if (data.success) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "Added to Cart!",
            showCancelButton: true,
            confirmButtonText: "See Cart",
            cancelButtonText: "Continue Shopping"
          }).then(result => {
            if (result.isConfirmed) {
              navigate("/cart");
            } else {
              navigate("/products");
            }
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: data.message
          })
        }

      })
  }

    useEffect(() => {
    fetch(`https://capstone-3-bautista-backend.onrender.com/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        setName(data.name)
        setDescription(data.description)
        setPrice(data.price)
        setIsActive(data.isActive) // set the state of isActive based on the API response
      })
      
      if (user) {
        setUserId(user.id); // set the user ID state using the user object
      }
      
  }, [productId, user])

  console.log("userId:", userId);
  console.log("userId:", userId);
  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Form.Group>
                <Form.Label>Quantity</Form.Label>
                <Form.Control type="number" min={1} value={quantity} onChange={(e) => setQuantity(e.target.value)} />
              </Form.Group>
              {isActive ? // condition to check if the product is active
                (user.id !== null ?
                  <Button variant="primary" className="mt-3"  onClick={() => order(productId, quantity)}>Add to Cart</Button> :
                  <Link className="btn btn-danger mt-3" to="/login">Log in to purchase.</Link>) :
                <Button variant="secondary" disabled>Not Available</Button> // disable the button if product is not active
              }
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}
