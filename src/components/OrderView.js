import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function OrderView() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();
  const { orderId } = useParams();
  const [userId, setUserId] = useState("");
  const [totalAmount, setTotalAmount] = useState("");
  const [purchasedOn, setPurchasedOn] = useState("");
  const [products, setProducts] = useState([]);
  const [isPaid, setIsPaid] = useState(false);

  useEffect(() => {
    fetch(`https://capstone-3-bautista-backend.onrender.com/orders/${orderId}`)
      .then(res => res.json())
      .then(data => {
        setUserId(data.userId)
        setTotalAmount(data.totalAmount)
        setPurchasedOn(data.purchasedOn)
        setIsPaid(data.isPaid)
        setProducts(data.products) // set the state of isActive based on the API response
      })
  }, [orderId])

  const handleAddProduct = () => {
    navigate(`/products?orderId=${orderId}`);
  }

  return (
    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>ORDER</Card.Title>
              <Card.Title className="text-start" style={{ textIndent: "20px" }}>{orderId}</Card.Title>
              <Card.Title>Total Amount</Card.Title>
              <Card.Text className="text-start" style={{ textIndent: "20px" }}>
                PHP {totalAmount}.00
              </Card.Text>
              <Card.Title>Purchased On</Card.Title>
              <Card.Text className="text-start" style={{ textIndent: "20px" }}>
                {purchasedOn}
              </Card.Text>
              <Card.Title>Status</Card.Title>
              <Card.Text className="text-start" style={{ textIndent: "20px" }}>
                {isPaid ? "Paid" : "Pending"}
              </Card.Text>
              <Card.Title>Products</Card.Title>
              <ul>
                {products.map((product) => (
                  <li key={product._id}>
                    <p>Product ID: {product.productId}</p>
                    <p>Quantity: {product.quantity}</p>
                    <p>Subtotal: PHP {product.subtotal}.00</p>
                  </li>
                ))}
              </ul>
              <Button variant="primary" onClick={handleAddProduct}>Add more products</Button>
              {/*<Link className="btn btn-danger" to="/cancelorder">Cancel Order</Link>*/}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

