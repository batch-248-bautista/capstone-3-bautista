import { Link } from 'react-router-dom'
import {Row, Col, Card, Button} from 'react-bootstrap';


export default function OrderCard ({ orderProp }) {
  const {totalAmount, purchasedOn, _id } = orderProp;

  return (
    <Row className="mt-3 mb-3">
        <Col> 
          <Card className="cardHighlight p-3">
            <Card.Body>
              <Card.Title>Order ID:</Card.Title>
              <Card.Text className="text-start" style={{ textIndent: "20px" }}>
                {_id}
              </Card.Text>
              <Card.Subtitle>Total Amount:</Card.Subtitle>
              <Card.Text className="text-start" style={{ textIndent: "20px" }}>
                PHP {totalAmount}.00
              </Card.Text>
              <Card.Subtitle>Date:</Card.Subtitle>
              <Card.Text className="text-start" style={{ textIndent: "20px" }}>
                {purchasedOn}
              </Card.Text>
              <Button className="bg-primary" as={Link} to={`/orders/${_id}`}>See Order</Button>
            </Card.Body>
          </Card>
      </Col>
  </Row>
  )
}

